public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("John Doe", "+639152468596", "+639228547963", "QuezonCity", "Makati City");
        Contact contact2 = new Contact("Jane Doe", "+639162148573", "+639173698541", "Caloocan City", "Pasay City");
        phonebook.addContact(contact1);
        phonebook.addContact(contact2);
        if (phonebook.isEmpty()) {
            System.out.println("Phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()) {
                System.out.println(contact.toString());
            }
        }
    }
}
