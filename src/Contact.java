public class Contact {
    private String name;
    private String contactNumber1;
    private String contactNumber2;
    private String home;
    private String office;

    public Contact() {}

    public Contact(String name, String contactNumber1, String contactNumber2, String home, String office) {
        this.name = name;
        this.contactNumber1 = contactNumber1;
        this.contactNumber2 = contactNumber2;
        this.home = home;
        this.office = office;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNumber1() {
        return contactNumber1;
    }

    public void setContactNumber1(String contactNumber1) {
        this.contactNumber1 = contactNumber1;
    }

    public String getContactNumber2() {
        return contactNumber2;
    }

    public void setContactNumber2(String contactNumber2) {
        this.contactNumber2 = contactNumber2;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    @Override
    public String toString() {
        return name + "\n-------------------------" + "\n" + name + " has the following registered numbers: " + "\n" + contactNumber1 + "\n" + contactNumber2 + "\n-------------------------------" + "\n" + name + " has the following registered addresses:" + "\nmy home in " + home + "\nmy office in " + office + "\n===========================================";
    }
}
